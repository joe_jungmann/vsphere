'use strict';
window.VsHelper = function() {
    var service,
        callbacks = [];

     
    var logout = function() {
        return service.vimPort.logout(service.serviceContent.sessionManager);
    };

    var getItems = function(kind){
          return service.vimPort.createContainerView(service.serviceContent.viewManager, service.serviceContent.rootFolder, [kind], true).then(function(containerView) {
            return service.vimPort.retrievePropertiesEx(service.serviceContent.propertyCollector, [
                service.vim.PropertyFilterSpec({
                    objectSet: service.vim.ObjectSpec({
                        obj: containerView,
                        selectSet: service.vim.TraversalSpec({
                            skip: false,
                            path: "view",
                            type: "ContainerView"
                        })
                    }),
                    propSet: service.vim.PropertySpec({
                        type: kind,
                        all: false,
                        pathSet: ["name"]
                    })
                })
            ], service.vim.RetrieveOptions());
        });
    };

    var sshStatus = function() {
        var propertyCollector = service.serviceContent.propertyCollector;
        var rootFolder = service.serviceContent.rootFolder;
        var viewManager = service.serviceContent.viewManager;
        var type = "HostSystem";
        return service.vimPort.createContainerView(viewManager, rootFolder, [type], true).then(function(containerView) {
            return service.vimPort.retrievePropertiesEx(propertyCollector, [
                service.vim.PropertyFilterSpec({
                    objectSet: service.vim.ObjectSpec({
                        obj: containerView,
                        skip: true,
                        selectSet: service.vim.TraversalSpec({
                            path: "view",
                            type: "ContainerView"
                        })
                    }),
                    propSet: service.vim.PropertySpec({
                        type: type,
                        all: false,
                        pathSet: ["config.service.service"]
                    })
                })
            ], service.vim.RetrieveOptions());
        }).then(function(result) {
            var items = result.objects[0].propSet[0].val;
            for(var x in items){
              if(items[x].key === 'TSM-SSH'){
                return items[x].running;
              }
            }
            
        });
    };

    var sshEnable = function(enable) {
        return service.vimPort[enable ? 'startService' : 'stopService']
            (service.vim.ManagedObjectReference({ type: 'HostServiceSystem', value: 'serviceSystem' }), service.xs.String({ value: 'TSM-SSH' }));
    };

    var callCallbacks = function(){
      while(callbacks.length > 0){
        callbacks.pop()();
      }
    };

    var initService = function() {
        console.log('initializing service');
        return vsphere.vimService('localhost.localdomain', { proxy: true }).
        then(function(vimService) {
            console.log(vimService);
            service = vimService;
            callCallbacks();
            return getItems('VirtualMachine');
        });         
    };


    var login = function(username, password) {
        return service.vimPort.login(service.serviceContent.sessionManager, username, password);
    };

    return {
      ready: function(callback){
        if(service){
          callback();
          return;
        }

        callbacks.push(callback);
      },
      sshStatus: sshStatus,
      sshEnable: sshEnable,
      init: initService,
      logout: logout,
      login: login,
      getVirtualMachines: function() {
        return getItems('VirtualMachine');
      },
      getDataStores: function(){
        return getItems('Datastore');
      }

    };
};
