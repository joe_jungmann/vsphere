'use strict';

angular.module('myApp.dash', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/dash', {
        templateUrl: 'dash/dash.html',
        controller: 'DashCtrl'
    });
}])

.controller('DashCtrl', ['$scope', '$location', function($scope, $location) {
	var states = ['Enable', 'Disable'],
	    sshStatus;
        
	$scope.btnLabel = 'Fetching';
    $scope.vms = [];
    $scope.ds = [];
    $scope.sshReady = false;

    serviceHelper.ready(function() {

		bindLabel();
        var bindScope = function(el){
            return function(data){
                $scope[el] = data.objects;
                $scope.$apply();
            };
        };

        serviceHelper.getDataStores().then(bindScope('ds'));
        serviceHelper.getVirtualMachines().then(bindScope('vms'));
    });

    var bindLabel = function(){
        serviceHelper.sshStatus().then(function(status){ 
            $scope.btnLabel = states[+status];
            $scope.sshReady = true;
            sshStatus = status;
            $scope.$apply();
        });
    };

    /**
     * [toggle description]
     * @return {[type]} [description]
     */
    $scope.toggle = function(){
        serviceHelper.sshEnable(!sshStatus).then(bindLabel);
    };


    /**
     * [logout description]
     * @return {[type]} [description]
     */
    $scope.logout = function() {
        serviceHelper.logout().then(function() {
            $location.path('/login');
        });
    };



}]);
