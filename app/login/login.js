'use strict';

angular.module('myApp.login', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/login', {
        templateUrl: 'login/login.html',
        controller: 'LoginCtrl'
    });
}])

.controller('LoginCtrl', ['$scope', '$location', function($scope, $location) {
    $scope.vm = {
    	errorMessage: null,
        dataLoading: false,
        login: function() {
            this.dataLoading = true;
            serviceHelper.login(this.username, this.password).then(function() {
                    $scope.vm.dataLoading = false;
                    $scope.$apply();
                    $location.path('/dash');
                })
                .catch(function(e) {
                	$scope.vm.errorMessage = e.message;
                	console.log(e);
                    $scope.vm.dataLoading = false;
                    $scope.$apply();
                });

        }
    };

}]);
