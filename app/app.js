'use strict';

window.serviceHelper = new VsHelper();

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.login',
  'myApp.dash',
  'myApp.version'
])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/login'});
   serviceHelper.init().then(function(err){
  		console.log(err);
  });
}]);
	